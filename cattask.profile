<?php

/**
 * @file
 * Enables modules and site configuration for the CatTask profile.
 */
use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements hook_install_tasks().
 */
function cattask_install_tasks(&$install_state) {
  $tasks = [
    '_cattask_final_site_setup' => [
      'display_name' => t('Import data'),
      'type' => 'batch',
      'display' => TRUE,
    ]
  ];
  return $tasks;
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function cattask_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Add a placeholder as example that one can choose an arbitrary site name.
  $form['site_information']['site_name']['#attributes']['placeholder'] = t('Your Commerce site name');
  // Add 'Social' fieldset and options.
  $form['cattask'] = [
    '#type' => 'fieldgroup',
    '#title' => t('CatTask Demo data'),
    '#description' => t('If you want to have demo data within your new site, please check the yes.'),
    '#weight' => 50,
  ];

  // Checkboxes to generate demo content.
  $form['cattask']['demo_content'] = [
    '#type' => 'checkbox',
    '#title' => t('Generate demo content'),
    '#description' => t('Will generate files, products, adverts.'),
  ];

  $form['#submit'][] = 'cattask_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function cattask_form_install_configure_submit($form, FormStateInterface $form_state) {
  \Drupal::state()->set('cattask_install_demo_content', $form_state->getValue('demo_content'));
}

function _cattask_final_site_setup(array &$install_state) {
  \Drupal::logger('cattask_demo')->notice('已经执行了 _cattask_final_site_setup()');
  $batch_operations = [];
  $demo_content = \Drupal::state()->get('cattask_install_demo_content');
  \Drupal::logger('cattask_demo')->notice('$demo_content的取值为：'.$demo_content);
  if ($demo_content === 1) {
    $batch_operations[] = ['_cattask_demo_execute_migrations', ['import']];
  }

  return [
    'title' => t('Importing data'),
    'operations' => $batch_operations,
    'finished' => '_cattask_update_site_info'
  ];
}

function _cattask_update_site_info() {
  // fix front page config
  $site_config = \Drupal::configFactory()->getEditable('system.site');
  /** @var \Drupal\path_alias\AliasManagerInterface $aliasManager */
  $aliasManager = \Drupal::service('path_alias.manager');
  $site_config
    ->set('page.front', $aliasManager->getPathByAlias($site_config->get('page.front')))
    ->save();
}


function cattask_locale_translation_projects_alter(&$projects) {
  // The translations are located at a custom translation sever
  $module_handler = \Drupal::service('module_handler');
  $module_path = $module_handler->getModule('cattask')->getPath();
  $projects['cattask']['info']['interface translation server pattern']
    = $module_path . '/' . $projects['cattask']['info']['interface translation server pattern'];
}
